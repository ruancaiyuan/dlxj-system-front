# dlxj-system-front

#### 介绍

Dlxj front end

#### 软件架构
* Bootstrap
    * Bootstrap, from twitter, is currently the most popular front-end framework. Bootstrap is based on HTML, CSS and JavaScript. It is concise and flexible, which makes web development faster.
      
      This tutorial will show you the basics of the bootstrap framework, which will make it easy for you to create web projects. The tutorial is divided into several parts: bootstrap basic structure, bootstrap CSS, bootstrap layout component and bootstrap plug-in. Each section contains simple and useful examples related to the topic.